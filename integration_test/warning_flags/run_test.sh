#!/bin/bash -e

script_dir=$(dirname "$0")
cmake_utils_package="${script_dir}/../.."
integration_test_package="${script_dir}"

echo "Export current package for this test"
conan export ${cmake_utils_package} cmake_utils/integration_test@user/testing

echo "Create test package and expect default WERROR to be OFF"
conan create ${integration_test_package} -o expect_werror=OFF --build=missing

echo "Manually build test package and expect default WERROR to be ON"
conan install ${integration_test_package} -if build -o expect_werror=ON
conan build ${integration_test_package} -bf build
