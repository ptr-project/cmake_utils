import os

from conans import ConanFile, CMake, tools


class WarningFlagsIntegrationTestConan(ConanFile):
    name = "cmake_utils_integration_test"
    version = "9001"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    requires = ["cmake_utils/integration_test@user/testing"]
    exports_sources = ["*.txt"]
    options = {"expect_werror": ["OFF", "ON"]}

    def build(self):
        cmake = CMake(self)
        cmake.configure(defs={"EXPECT_WERROR": self.options.expect_werror})
