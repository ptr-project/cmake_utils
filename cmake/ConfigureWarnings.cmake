# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

include_guard(GLOBAL)

include(CheckCXXCompilerFlag)

if (CONAN_IN_LOCAL_CACHE)
  set(werror_default OFF)
else()
  set(werror_default ON)
endif()

option(WARNING_WERROR "Treat all warnings as errors" ${werror_default})

message(STATUS "Treating warnings as errors: ${WARNING_WERROR}")

function(add_flag_if_supported warning_flag cache_variable)
  # Based on https://stackoverflow.com/a/52577244/522251
  # we want negative options like "-Wno-c99-extensions" to be checked if they
  # are valid by their positive counterparts, e.g., "-Wc99-extensions"
  if (warning_flag MATCHES "^-Wno-")
    string(SUBSTRING "${warning_flag}" 5 -1 positive_flag)
    set(check_flag "-W${positive_flag}")
  else()
    set(check_flag "${warning_flag}")
  endif()

  check_cxx_compiler_flag("${check_flag}" ${cache_variable})

  if(${cache_variable})
    add_compile_options(${warning_flag})
  endif()
endfunction()

add_flag_if_supported("-Wall" WARNING_HAS_ALL_WARNINGS)
add_flag_if_supported("-Wextra" WARNING_HAS_EXTRA)
add_flag_if_supported("-Wshadow" WARNING_HAS_SHADOW)
add_flag_if_supported("-Wnon-virtual-dtor" WARNING_HAS_NON_VIRTUAL_DTOR)
add_flag_if_supported("-Wold-style-cast" WARNING_HAS_OLD_STYLE_CAST)
add_flag_if_supported("-Wcast-align" WARNING_HAS_CAST_ALIGN)
add_flag_if_supported("-Wunused" WARNING_HAS_UNUSED)
add_flag_if_supported("-Wunused-variable" WARNING_HAS_UNUSED_VARIABLE)
add_flag_if_supported("-Woverloaded-virtual" WARNING_HAS_OVERLOADED_VIRTUAL)
add_flag_if_supported("-Wpedantic" WARNING_HAS_PEDANTIC)
add_flag_if_supported("-Wconversion" WARNING_HAS_CONVERSION)
add_flag_if_supported("-Wsign-conversion" WARNING_HAS_SIGNCONVERSION)
add_flag_if_supported("-Wnull-dereference" WARNING_HAS_NULLDEREFERENCE)
add_flag_if_supported("-Wdouble-promotion" WARNING_HAS_DOUBLE_PROMOTION)
add_flag_if_supported("-Wformat=2" WARNING_HAS_FORMAT)
add_flag_if_supported("-Wduplicated-cond" WARNING_HAS_DUPLICATED_COND)
add_flag_if_supported("-Wduplicated-branches" WARNING_HAS_DUPLICATED_BRANCHES)
add_flag_if_supported("-Wlogical-op" WARNING_HAS_LOGICAL_OP)
add_flag_if_supported("-Wuseless-cast" WARNING_HAS_USELESS_CAST)
add_flag_if_supported("-Wsign-conversion" WARNING_HAS_SIGN_CONVERSION)
add_flag_if_supported("-Winconsistent-missing-override" WARNING_HAS_INCONSISTENT_MISSING_OVERRIDE)
add_flag_if_supported("-Wignored-qualifiers" WARNING_HAS_IGNORED_QUALIFIERS)

if(WARNING_WERROR)
  message(STATUS "Treating all warnings as errors")
  add_flag_if_supported("-Werror" WARNING_HAS_ERROR)
endif()

add_flag_if_supported("-Wno-c99-extensions" WARNING_HAS_NO_C99_EXTENSIONS)
add_flag_if_supported("-Wno-error=pedantic" WARNING_HAS_NO_ERROR_PEDANTIC)
