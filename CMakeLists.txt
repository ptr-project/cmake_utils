cmake_minimum_required(VERSION 3.14)

project(cmake_utils)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

include(CTest)

if(BUILD_TESTING)
  add_subdirectory(tests)
  add_subdirectory(integration_test)
endif()
