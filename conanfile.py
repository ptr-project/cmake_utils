from conans import ConanFile, CMake, tools
import os.path


class CmakeUtilsConan(ConanFile):
    name = "cmake_utils"
    license = "MIT"
    author = "Kai Hoewelmeyer"
    url = "https://gitlab.com/ptr-project/cmake_utils"
    description = "CMake Helper Scripts"
    topics = ("cmake", "utils")
    generators = "cmake"
    no_copy_source = True
    options = {"build_testing": [True, False]}
    default_options = {"build_testing": False}
    exports_sources = ["cmake/*.cmake", "CMakeLists.txt", "tests/*"]

    def package(self):
        self.copy("*.cmake", keep_path=False, dst="cmake/")

    def package_info(self):
        self.cpp_info.builddirs = [os.path.join(self.package_folder, "cmake")]

    def build(self):
        cmake = CMake(self)
        cmake.configure(defs={"BUILD_TESTING": self.options.build_testing})
        cmake.build()
