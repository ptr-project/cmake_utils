# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

include_guard(GLOBAL)

function(compile_test)
    set(options EXPECT_FAILURE)
    set(oneValueArgs NAME)
    set(multiValueArgs SOURCES)
    cmake_parse_arguments(TEST_ARGS "${options}" "${oneValueArgs}"
        "${multiValueArgs}" ${ARGN} )

    add_executable(${TEST_ARGS_NAME} EXCLUDE_FROM_ALL ${TEST_ARGS_SOURCES})
    set(bang "")
    if(${TEST_ARGS_EXPECT_FAILURE})
        set(bang "!")
    endif()
    add_test(NAME ${TEST_ARGS_NAME}
        COMMAND bash -c "${bang} ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR} --target ${TEST_ARGS_NAME}")
endfunction()

